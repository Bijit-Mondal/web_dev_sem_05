import java.util.*;
public class MatrixMultiplication{
    public static void main(String[]args){
        int[][]matrix1;
        int[][]matrix2;
        int[][]ans;
        int row1,col1,row2,col2;
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number of rows of matrix 1");
        row1=sc.nextInt();
        System.out.println("Enter the number of columns of matrix 1");
        col1=sc.nextInt();
        System.out.println("Enter the number of rows of matrix 2");
        row2=sc.nextInt();
        System.out.println("Enter the number of columns of matrix 2");
        col2=sc.nextInt();
        if(col1!=row2){
            System.out.println("Matrix multiplication not possible");
            System.exit(0);
        }
        matrix1=new int[row1][col1];
        matrix2=new int[row2][col2];
        ans=new int[row1][col2];
        System.out.println("Enter the elements of matrix 1");
        for(int i=0;i<row1;i++){
            for(int j=0;j<col1;j++){
                matrix1[i][j]=sc.nextInt();
            }
        }
        System.out.println("Enter the elements of matrix 2");
        for(int i=0;i<row2;i++){
            for(int j=0;j<col2;j++){
                matrix2[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<row1;i++){
            for(int j=0;j<col2;j++){
                for(int k=0;k<row2;k++){
                    ans[i][j]+=matrix1[i][k]*matrix2[k][j];
                }
            }
        }
        System.out.println("Matrix 1 ");
        for(int i=0;i<row1;i++){
            for(int j=0;j<col1;j++){
                System.out.print(matrix1[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("Matrix 2 ");
        for(int i=0;i<row2;i++){
            for(int j=0;j<col2;j++){
                System.out.print(matrix2[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println("Matrix multiplication ");
        for(int i=0;i<row1;i++){
            for(int j=0;j<col2;j++){
                System.out.print(ans[i][j]+" ");
            }
            System.out.println();
        }
    }
}


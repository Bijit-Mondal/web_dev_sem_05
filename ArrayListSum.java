import java.util.*;
public class ArrayListSum{
    public static boolean numberOrNot(String input){
        try{
            Integer.parseInt(input);
        }
        catch(NumberFormatException ex){
            return false;
        }
        return true;
    }
    public static void main(String[]args){
        ArrayList<Integer> list=new ArrayList<Integer>();
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter element in ArrayList, enter any text to exit");
        String n=sc.next();
        int sum=0;
        while(numberOrNot(n)){
            list.add(Integer.parseInt(n));
            n=sc.next();
        }
        for(int number : list){
            sum += number;
        }
        System.out.println("The sum is "+sum);
    }
}
//Write a program to enter few integer elements in an array and find the largest and smallest element present in that array.
import java.util.Scanner;
public class LargestSmallest{
    public static void main(String[]args){
        int n;
        System.out.println("Enter the number of elements in the array");
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();
        int[]arr=new int[n];
        for(int i=0;i<n;i++){
            System.out.print("Enter the element ["+(i+1)+"] ");
            arr[i]=sc.nextInt();
        }
        int max=arr[0];
        int min=arr[0];
        for(int i=1;i<n;i++){
            if(arr[i]>max){
                max=arr[i];
            }
            else if(arr[i]<min){
                min=arr[i];
            }
        }
        System.out.println("The largest element in the array is "+max);
        System.out.println("The smallest element in the array is "+min);
    }
}


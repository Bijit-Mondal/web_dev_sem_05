//Write a program to insert few integer values in an ArrayList,add 5 to each element and print.
import java.util.*;
public class ArrayList01{
    public static void main(String[]args){
        ArrayList<Integer> list=new ArrayList<Integer>();
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter element in ArrayList, enter -0 to exit");
        int n=sc.nextInt();
        while(n!=-1){
            list.add(n);
            n=sc.nextInt();
        }
        for(int i=0;i<list.size();i++){
            list.set(i,list.get(i)+5);
        }   
        System.out.println(list);
    }
}
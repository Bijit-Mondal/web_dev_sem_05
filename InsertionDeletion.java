import java.util.*;

public class InsertionDeletion{

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the max number of elements in the array");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for(int i = 0; i < size; i++){
            array[i] = 0;
        }
        int noOfElements;
        System.out.println("Enter the number of elements less than size of array you want to insert");
        noOfElements = scanner.nextInt();
        System.out.println("Enter the elements of the array: ");
        for (int i = 0; i < noOfElements; i++) {
            array[i] = scanner.nextInt();
        }
        while(true){
            System.out.println("Enter 1 for insertion, 2 for deletion, 3 for display and 4 for exit");
            int choice = scanner.nextInt();
            switch(choice){ 
                case 1:
                    System.out.println("Enter the index and element to be inserted: ");
                    int index = scanner.nextInt();
                    int element = scanner.nextInt();
                    if(index>=array.length){
                        System.out.println("Invalid index");
                    }
                    else{
                        insertElement(array, index, element);
                    }
                    System.out.println("Array after inserting element: " + Arrays.toString(array));
                    break;
                case 2:
                    System.out.println("Enter the index of the element to be deleted: ");
                    index = scanner.nextInt();
                    if(index>=array.length){
                        System.out.println("Invalid index");
                    }
                    else{
                        deleteElement(array, index);
                    }
                    System.out.println("Array after deleting element: " + Arrays.toString(array));
                    break;
                case 3:
                    System.out.println("Array: " + Arrays.toString(array));
                    break;
                case 4:
                    System.exit(0);
            }
        }
    }

    private static void insertElement(int[] array, int index, int element) {
        for (int i = array.length - 1; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = element;
    }

    private static void deleteElement(int[] array, int index) {
        for (int i = index; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        array[array.length - 1] = 0;
    }
}


import java.util.*;
import java.lang.*;
public class InsertionDeletionArrayList{
    public static void main(String[]args){
        ArrayList<Integer> list = new ArrayList<Integer>();
        Scanner sc=new Scanner(System.in);
        int maxIndex=0;
        int element,index;
        while(true){
            System.out.println("Enter 1 for insertion, 2 for deletion, 3 for display and 4 for exit");
            int choice = sc.nextInt();
            switch(choice){ 
                case 1:
                    System.out.print("Enter an element to insert in arraylist ");
                    element = sc.nextInt();
                    System.out.print("Enter the index to insert in arraylist ");
                    index = sc.nextInt();
                    try{
                        list.add(index,element);
                    }catch(IndexOutOfBoundsException ex){
                        System.out.println(ex);
                    }
                    break;
                case 2:
                    System.out.print("Enter the index to remove in arraylist ");
                    index = sc.nextInt();
                    try{
                        list.remove(index);
                    }catch(IndexOutOfBoundsException ex){
                        System.out.println(ex);
                    }
                    break;
                case 3:
                    System.out.println(list);
                    break;
                case 4:
                    System.exit(0);
            }
        }
    }
}
import all.mine.my;
import all.your.your;
public class Main{
    public static void main(String[]args){
        my myObj = new my();
        your yourObj = new your();
        my.me();
        myObj.myWorld();
        yourObj.yourWorld();
    }
}
//jar cvfe Main.jar Main Main.class all/mine/my.class all/your/your.class

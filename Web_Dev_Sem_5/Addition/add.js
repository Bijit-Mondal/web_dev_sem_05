let calculatorForm = document.getElementById('calculatorForm');
calculatorForm
  .addEventListener("submit", function (event) {
    event.preventDefault();

    const number1 = parseFloat(document.getElementById("number1").value);
    const number2 = parseFloat(document.getElementById("number2").value);
    const operation = document.getElementById("operation").value;
    let result;
    if (isNaN(number1) || isNaN(number2)) {
        calculatorForm.reset();
        alert("Please enter a number");
        return;
    }
    switch (operation) {
      case "+":
        result = number1 + number2;
        break;
      case "-":
        result = number1 - number2;
        break;
      case "*":
        result = number1 * number2;
        break;
      case "/":
        result = number1 / number2;
        break;
    }

    const resultSpan = document.getElementById("result");
    resultSpan.textContent = result;
    const resultDiv = document.getElementById("result_div");
    resultDiv.style.display = "block";
});

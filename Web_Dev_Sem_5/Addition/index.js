let submit = document.getElementById("submit");
submit.addEventListener("click", function () {
  let number1 = document.getElementById("number1").value;
  let number2 = document.getElementById("number2").value;
  if (isNaN(number1) || isNaN(number2)) {
    alert("Please enter a number");
    return;
  }
  let operation = document.getElementById("operation").value;
  let result = 0;
  if (operation == "+") {
    result = parseInt(number1) + parseInt(number2);
  } else if (operation == "-") {
    result = parseInt(number1) - parseInt(number2);
  } else if (operation == "*") {
    result = parseInt(number1) * parseInt(number2);
  } else if (operation == "/") {
    result = parseInt(number1) / parseInt(number2);
  }
  //show the result in the result div
  document.getElementById("result").innerHTML = result;
  document.getElementById("result_div").style.display = "block";
});
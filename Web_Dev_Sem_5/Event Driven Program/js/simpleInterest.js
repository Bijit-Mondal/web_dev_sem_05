// A person deposit Rs 1000 in a fixed account yielding 5% interest compute the amount in the account at the end of each year for n year in JavaScript.
const calculate = () =>{
    const starting_amount = document.getElementById("capital_amount").value;
    const interest_rate = document.getElementById("interest_amount").value * 0.01;
    const result_div = document.getElementById("result_div");
    let year = document.getElementById("year").value;

    if(starting_amount <= 0 || interest_rate <= 0 || year <= 0){
        result_div.innerHTML = "Please enter a positive number";
        return;
    }
    let final_amount = 0;

    const calculateInterest = (year) =>{
        final_amount = starting_amount * (1 + interest_rate) ** year;
        return final_amount;
    }
    for(let i = 1; i <= year; i++){
        result_div.innerHTML += `<p>The amount in the account at the end of ${i} year is ${calculateInterest(i)}<p>`;
    }
}

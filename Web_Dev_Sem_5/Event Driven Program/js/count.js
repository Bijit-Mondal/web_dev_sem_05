//Create an event driven program to read n numbers and count the number of negative numbers, positive numbers and zeros. and print the count.
//Take the number of input from the user
var positive = 0;
var negative = 0;
var zero = 0;
const fn = () =>{
    var n = Number(prompt("Enter the number of inputs"));
    //Check whether n is number or not and also check if user has input anything or not
    if(isNaN(n) || n == ""){
        alert("You are fired");
        fn();
    }
    for (var i = 0; i < n; i++) {
        var num = prompt("Enter the number");
        //num can be either positive, negative or zero so check whether it is something else or blank or not a number
        if(isNaN(num) || num == ""){
            alert("enter a number");
            i--;
            continue;
        }
        num = Number(num);
        if (num > 0) {
            positive++;
        }
        else if (num < 0) {
            negative++;
        }
        else {
            zero++;
        }
    }
    document.write("The number of positive numbers are " + positive + "<br>The number of negative numbers are " + negative + "<br>The number of zeros are " + zero);
}
const menuIcon = document.querySelector(".menu_icon");
const toggleMenu = () => {
    const menuCandy = document.querySelector(".menu_candy");
    const menuContent = document.querySelectorAll(".menu_content");
    if (menuCandy.classList.contains("menu_active")) {
        menuCandy.classList.remove("menu_active");
    } else {
        menuCandy.classList.add("menu_active");
    }
    menuContent.forEach((content) => {
        content.classList.toggle("menu_content_display")
    });
};
menuIcon.addEventListener("click", toggleMenu);

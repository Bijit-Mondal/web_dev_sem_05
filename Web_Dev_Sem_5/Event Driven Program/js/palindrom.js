let inputForm = document.getElementById('input_form');
inputForm
  .addEventListener("submit", function (event) {
    event.preventDefault();
    const str = document.getElementById('input_string').value;
    //check if string is palindrome or not irrsepective of case
    if (str.toLowerCase().replace(/\s/g,"") === str.toLowerCase().replace(/\s/g,"").split('').reverse().join('')) {
        document.getElementById('result').innerHTML = `${str} is a palindrome`;
    } else {
        document.getElementById('result').innerHTML = `${str} is not a palindrome`;
    }
  });
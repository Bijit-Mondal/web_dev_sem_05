//write a function to return square of a number
const fn = (n, pow) => {
  return n ** pow;
};
const res = document.getElementById("res");
let template;
for (var i = 5; i <= 15; i++) {
  template = `<div class="flex-container brand align-center flex-item-center">
                    <div class="flex-container flex-item-4 align-center">
                        <p>${i} </p>
                    </div>
                    <div class="flex-container flex-item-4 align-center">
                        <p>${fn(i, 2)} </p>
                    </div>
                    <div class="flex-container flex-item-4 align-center">
                        <p>${fn(i, 3)} </p>
                    </div>
                </div>
                `;
  res.innerHTML += template;
}
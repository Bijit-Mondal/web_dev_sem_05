//ask for size of the array from user and create an array of that size, add positive number to the array ending with 0 and if the number is not valid, show a error message and take the valid input again. and show the sum
const fn = () =>{
  var size = prompt("Enter the size of the array");
  var arr = new Array(size);
  var sum = 0;
  for (var i = 0; i < size; i++) {
      arr[i] = prompt("Enter the number");
      if (isNaN(arr[i]) || arr[i]%10 || arr[i]<=0) {
          alert("Enter a valid number");
          i--;
      }
      else {
          sum += parseInt(arr[i]);
      }
  }
  alert("The sum of the array is " + sum);
}

fn();
let sum = 0;
let count = 0;
let num = 1;
const fn = () =>{
    while (num != 0) {
        num = parseInt(prompt("Enter a number: "));
        if(isNaN(num) || num < 0) {
            alert("Please enter a valid number");
            continue;
        }
        sum += num;
        count++;
    }
    console.log("Sum = " + sum);
    console.log("Average = " + sum/count);
}
fn();